# Technical Challenge
This Java Spring Boot application is a solution for a technical challenge, focusing on a single key functionality: rounding up transactions and transferring the rounded-up amount into a specified saving goal. Integrated with Starling Bank Public APIs, this application provides a streamlined and efficient method for managing savings.
# Prerequisites
    Maven
    Starling Bank Sandbox API access token

# Configuration
    you need to configure the Starling Bank Sandbox API access token in the application.properties file.
    bearerToken = [Your Starling Bank Sandbox API Access Token]
    server.port=[Port you want to the app to run on i.e 8080, 8084]

# Running the Application
To run the application locally, follow these steps:
Clone this repository
Build the project using Maven: mvn clean install
Run the application using Maven: mvn spring-boot:run
The application will start and the endpoint will be accessible via postman at GET /starling-bank-savings/round-up

# Usage

### API Endpoints

#### Round-Up Transactions

- **Round-Up Transactions**
    - Endpoint: `GET /starling-bank-savings/round-up`
    - Description: Round up balance.
    - Parameters:
        - `savingGoalName` (optional): The name of the saving goal. If not provided, a random UUID will be used.
    - Response:
        - Success:
            ```json
            {
              "savingsGoalUid": "6c2b756a-19a5-4a4d-af7b-7f1a7c54d167",
              "name": "testSavingGoal5",
              "target": null,
              "totalSaved": {
                "currency": "GBP",
                "minorUnits": 1200
              }
            }
            ```
        - Error:
            ```json
            {
              "error": "An error occurred while rounding up: <error message>"
            }
            ```
# Enhancements
With more time, additional features could be incorporated into the application. For instance, allowing users to specify a date parameter in the API endpoint would enable rounding up transactions from the Monday to Sunday of that particular week. Additionally, implementing validation for scenarios where there are no transactions available for the specified date range would enhance the user experience.

Another valuable enhancement would involve enabling users to update existing saving goals by introducing a savingsGoalId request parameter to the endpoint. This functionality would provide users with the flexibility to modify and manage their saving goals more effectively.

Expanding test coverage across various scenarios and edge cases would also be beneficial, ensuring robustness and reliability of the application.

Integrating Swagger documentation would enhance the usability of the API, providing comprehensive documentation for developers to interact with the endpoints seamlessly.

Furthermore, the addition of a Dockerfile would facilitate the deployment and scaling of the application in containerized environments, simplifying the setup and management of the application infrastructure.