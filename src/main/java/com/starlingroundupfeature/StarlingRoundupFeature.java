package com.starlingroundupfeature;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class StarlingRoundupFeature {

    public static void main(String[] args) {
        SpringApplication.run(StarlingRoundupFeature.class, args);
    }
}