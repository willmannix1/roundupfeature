package com.starlingroundupfeature.configuration;

import com.starlingroundupfeature.reponse.account.AccountsResponse;
import com.starlingroundupfeature.reponse.amount.Amount;
import com.starlingroundupfeature.reponse.amount.AmountResponse;
import com.starlingroundupfeature.reponse.savinggoal.SavingGoalResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

@Configuration
public class HeadersConfig {

    private static final Logger LOG = LoggerFactory.getLogger(HeadersConfig.class);

    @Value("${bearerToken}")
    private String bearerToken;

    /**
     * Create headers.
     *
     * @return HttpHeaders
     */
    public HttpHeaders getHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "Bearer " + this.bearerToken);

        return headers;
    }

    /**
     * Retrieve custom headers for the saving goals API request.
     *
     * @return The HTTP entity containing the custom headers.
     */
    public HttpEntity<SavingGoalResponse> getSavingGoalsInfoCustomHeaders() {
        HttpHeaders httpHeaders = getHeaders();
        return new HttpEntity<>(httpHeaders);
    }


    /**
     * Retrieve custom headers for the account information API request.
     *
     * @return The HTTP entity containing the custom headers.
     */
    public HttpEntity<AccountsResponse> getAccountInfoCustomHeaders() {
        HttpHeaders httpHeaders = getHeaders();
        return new HttpEntity<>(httpHeaders);
    }

    /**
     * Retrieve custom headers for the amount API request.
     *
     * @param roundedUpValue The rounded-up value for the savings.
     * @return The HTTP entity containing the custom headers.
     */
    public HttpEntity<AmountResponse> getAmountCustomHeaders(final BigDecimal roundedUpValue) {
        HttpHeaders httpHeaders = getHeaders();

        AmountResponse money = new AmountResponse();
        Amount amount = new Amount();
        amount.setMinorUnits(roundedUpValue.movePointRight(2));
        amount.setCurrency(Currency.getInstance("GBP"));
        money.setAmount(amount);
        return new HttpEntity<>(money, httpHeaders);
    }

    /**
     * Get headers.
     *
     * @return HttpEntity
     */
    public HttpEntity<Void> getCustomHeaders() {
        final HttpHeaders httpHeaders = getHeaders();
        return new HttpEntity<>(httpHeaders);
    }
}