package com.starlingroundupfeature.controller;

import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.savinggoal.SavingGoalResponse;
import com.starlingroundupfeature.service.RoundUpBalanceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/starling-bank-savings")
public class RoundUpBalanceController {

    private final RoundUpBalanceService service;

    @Autowired
    public RoundUpBalanceController(RoundUpBalanceService service) {
        this.service = service;
    }

    @GetMapping("/round-up")
    @Operation(summary = "Round up balance")
    public ResponseEntity<?> roundUp(@RequestParam(required = false) String savingGoalName) {
        try {
            if (savingGoalName == null || savingGoalName.isEmpty()) {
                savingGoalName = UUID.randomUUID().toString();
            }
            SavingGoalResponse response = service.roundUp(savingGoalName);
            return ResponseEntity.ok(response);
        } catch (RoundUpException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("An error occurred while rounding up: " + e.getMessage());
        }
    }
}