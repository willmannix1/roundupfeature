package com.starlingroundupfeature.exception;

import lombok.Builder;
import lombok.Getter;

@Getter
public class RoundUpException extends Exception {

    private final String detailedMessage;

    @Builder
    public RoundUpException(String message, String detailedMessage, Throwable cause) {
        super(message, cause);
        this.detailedMessage = detailedMessage;
    }
}