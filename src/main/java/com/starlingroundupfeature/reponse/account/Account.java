package com.starlingroundupfeature.reponse.account;

import lombok.Data;

import java.util.Date;

@Data
public class Account {
    private String accountUid;
    private String defaultCategory;
    private String currency;
    private Date createdAt;

}