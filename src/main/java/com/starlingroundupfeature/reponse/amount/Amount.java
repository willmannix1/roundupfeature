package com.starlingroundupfeature.reponse.amount;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Currency;

@Data
public class Amount implements Serializable {

    private Currency currency;
    private BigDecimal minorUnits;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getMinorUnits() {
        return minorUnits;
    }

    public void setMinorUnits(BigDecimal minorUnits) {
        this.minorUnits = minorUnits;
    }
}