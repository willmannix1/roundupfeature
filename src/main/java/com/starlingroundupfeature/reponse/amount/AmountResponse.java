package com.starlingroundupfeature.reponse.amount;

import lombok.Data;

import java.io.Serializable;

@Data
public class AmountResponse implements Serializable {
    private Amount amount;
}
