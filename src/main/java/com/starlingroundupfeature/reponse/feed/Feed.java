package com.starlingroundupfeature.reponse.feed;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.starlingroundupfeature.reponse.amount.Amount;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Feed {
    @JsonProperty("feedItemUid")
    private String feedItemUid;
    @JsonProperty("categoryUid")
    private String categoryUid;
    @JsonProperty("direction")
    private String direction;
    @JsonProperty("amount")
    private Amount amount;
}