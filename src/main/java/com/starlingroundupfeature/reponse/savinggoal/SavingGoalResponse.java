package com.starlingroundupfeature.reponse.savinggoal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.starlingroundupfeature.reponse.amount.Amount;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SavingGoalResponse {

    @JsonProperty("savingsGoalUid")
    private String savingsGoalUid;
    @JsonProperty("name")
    private String name;
    @JsonProperty("target")
    private Amount target;
    @JsonProperty("totalSaved")
    private Amount totalSaved;
}