package com.starlingroundupfeature.reponse.savinggoal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SavingGoalsResponse {

    @JsonProperty("savingsGoalList")
    private List<SavingGoalResponse> savingGoals;
}