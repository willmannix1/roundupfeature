package com.starlingroundupfeature.reponse.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.starlingroundupfeature.reponse.feed.Feed;
import lombok.Data;

import java.util.List;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Transaction {
    @JsonProperty("feedItems")
    List<Feed> feedResponseList;
}
