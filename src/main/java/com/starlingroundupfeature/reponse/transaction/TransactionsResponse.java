package com.starlingroundupfeature.reponse.transaction;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionsResponse implements Serializable {
    private List<Transaction> transactions;
}
