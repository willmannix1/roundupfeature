package com.starlingroundupfeature.request.savinggoal;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Currency;

@Data
@Builder
public class SavingGoalRequest implements Serializable {
    private String name;
    private Currency currency;
}