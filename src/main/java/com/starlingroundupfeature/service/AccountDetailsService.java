package com.starlingroundupfeature.service;

import com.starlingroundupfeature.configuration.HeadersConfig;
import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.account.Account;
import com.starlingroundupfeature.reponse.account.AccountsResponse;
import com.starlingroundupfeature.util.LoggerUtils;
import com.starlingroundupfeature.util.StarlingApiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@Service
public class AccountDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(SavingGoalService.class);

    private final RestTemplate restTemplate;
    private final HeadersConfig headersConfiguration;
    private final LoggerUtils loggerUtils;

    @Autowired
    public AccountDetailsService(RestTemplate restTemplate, HeadersConfig headersConfiguration, LoggerUtils loggerUtils) {
        this.restTemplate = restTemplate;
        this.headersConfiguration = headersConfiguration;
        this.loggerUtils = loggerUtils;
    }


    /**
     * Retrieve the account UID associated with the user.
     *
     * @return The account UID.
     * @throws RoundUpException If an error occurs while retrieving the account UID or if no accounts are found.
     */
    public String getAccountUid() throws RoundUpException {
        try {
            HttpEntity<AccountsResponse> request = headersConfiguration.getAccountInfoCustomHeaders();
            ResponseEntity<AccountsResponse> responseEntity = restTemplate.exchange(
                    StarlingApiUtils.API_STARLING_BANK_ACCOUNT_INFO,
                    HttpMethod.GET,
                    request,
                    new ParameterizedTypeReference<AccountsResponse>() {
                    }
            );

            List<Account> accounts = Objects.requireNonNull(responseEntity.getBody()).getAccounts();
            if (accounts.isEmpty()) {
                String errorMessage = "No accounts found";
                logger.warn("{} for user", errorMessage);
                throw RoundUpException.builder()
                        .message(errorMessage)
                        .detailedMessage(errorMessage)
                        .build();
            }
            return accounts.get(0).getAccountUid();
        } catch (Exception e) {
            final String message = "Failed to retrieve account ID";
            loggerUtils.logException(logger, message, e);
            throw new RoundUpException(message, e.getMessage(), e);
        }
    }

    /**
     * Get category id.
     *
     * @return String with the category id
     * @throws RoundUpException if an error occurs while fetching the default category UID
     */
    public String getDefaultCategoryUid() throws RoundUpException {
        try {
            AccountsResponse response = getAccountInfo();
            return Objects.requireNonNull(response).getAccounts().get(0).getDefaultCategory();
        } catch (Exception e) {
            final String message = "Error fetching default category UID";
            loggerUtils.logException(logger, message, e);
            throw new RoundUpException(message, e.getMessage(), e);
        }
    }

    /**
     * Get account information with custom headers.
     *
     * @return {@link AccountsResponse} containing account information
     * @throws RoundUpException if an error occurs while fetching account information
     */
    protected AccountsResponse getAccountInfo() throws RoundUpException {
        try {
            final HttpEntity<Void> request = headersConfiguration.getCustomHeaders();
            return restTemplate.exchange(
                    StarlingApiUtils.API_STARLING_BANK_ACCOUNT_INFO,
                    HttpMethod.GET,
                    request,
                    new ParameterizedTypeReference<AccountsResponse>() {
                    }
            ).getBody();
        } catch (Exception e) {
            final String message = "Error fetching account information";
            loggerUtils.logException(logger, message, e);
            throw new RoundUpException(message, e.getMessage(), e);
        }
    }

}
