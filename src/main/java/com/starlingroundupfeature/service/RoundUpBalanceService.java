package com.starlingroundupfeature.service;

import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.feed.Feed;
import com.starlingroundupfeature.reponse.savinggoal.SavingGoalResponse;
import com.starlingroundupfeature.util.LoggerUtils;
import com.starlingroundupfeature.util.RoundUpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Service for handling round-up balance operations.
 */
@Service
public class RoundUpBalanceService {

    private static final Logger logger = LoggerFactory.getLogger(RoundUpBalanceService.class);

    private final TransactionService transactionService;
    private final SavingGoalService savingGoalService;
    private final RoundUpUtils roundUpUtils;
    private final LoggerUtils loggerUtils;

    @Autowired
    public RoundUpBalanceService(TransactionService transactionService, SavingGoalService savingGoalService, RoundUpUtils roundUpUtils, LoggerUtils loggerUtils) {
        this.transactionService = transactionService;
        this.savingGoalService = savingGoalService;
        this.roundUpUtils = roundUpUtils;
        this.loggerUtils = loggerUtils;
    }

    /**
     * Performs the round-up process.
     *
     * @param savingGoalName The name of the saving goal.
     * @return The saving goal response after round-up.
     * @throws RoundUpException If an error occurs during the round-up process.
     */
    public SavingGoalResponse roundUp(String savingGoalName) throws RoundUpException {
        try {
            // Get transactions
            List<Feed> transactions = transactionService.getTransactions();

            // Calculate rounded-up amount
            BigDecimal roundedUpAmount = roundUpUtils.calculate(transactions);

            // Create savings goal
            ResponseEntity<SavingGoalResponse> responseEntity = savingGoalService.createSavingsGoal(savingGoalName);
            SavingGoalResponse savingGoal = responseEntity.getBody();
            String savingsGoalUid = savingGoal.getSavingsGoalUid();

            // Put savings into the created goal
            savingGoalService.putSavings(savingsGoalUid, roundedUpAmount);

            // Get updated saving goal
            return savingGoalService.getSavingsGoals(savingsGoalUid);
        } catch (Exception e) {
            // Log and rethrow the exception as RoundUpException
            String message = "Error processing round-up transactions";
            loggerUtils.logException(logger, message, e);
            throw new RoundUpException(message, message, e);
        }
    }
}