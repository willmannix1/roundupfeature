package com.starlingroundupfeature.service;

import com.starlingroundupfeature.configuration.HeadersConfig;
import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.amount.AmountResponse;
import com.starlingroundupfeature.reponse.savinggoal.SavingGoalResponse;
import com.starlingroundupfeature.request.savinggoal.SavingGoalRequest;
import com.starlingroundupfeature.util.LoggerUtils;
import com.starlingroundupfeature.util.StarlingApiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class SavingGoalService {

    private static final Logger logger = LoggerFactory.getLogger(SavingGoalService.class);

    private final RestTemplate restTemplate;
    private final HeadersConfig headersConfiguration;
    private final AccountDetailsService accountDetailsService;
    private final LoggerUtils loggerUtils;

    @Autowired
    public SavingGoalService(RestTemplate restTemplate, HeadersConfig headersConfiguration, AccountDetailsService accountDetailsService, LoggerUtils loggerUtils) {
        this.restTemplate = restTemplate;
        this.headersConfiguration = headersConfiguration;
        this.accountDetailsService = accountDetailsService;
        this.loggerUtils = loggerUtils;
    }

    /**
     * Retrieve saving goals for a specified savings goal UID.
     *
     * @param savingsGoalUid The UID of the savings goal to retrieve.
     * @return The response containing the saving goals.
     * @throws RoundUpException If an error occurs while retrieving the saving goals.
     */
    public SavingGoalResponse getSavingsGoals(String savingsGoalUid) throws RoundUpException {
        try {
            String accountUid = accountDetailsService.getAccountUid();
            Map<String, String> params = new HashMap<>();
            params.put("accountUid", accountUid);
            params.put("savingsGoalUid", savingsGoalUid);

            HttpEntity<SavingGoalResponse> request = headersConfiguration.getSavingGoalsInfoCustomHeaders();
            ResponseEntity<SavingGoalResponse> responseEntity = restTemplate.exchange(
                    StarlingApiUtils.API_STARLING_BANK_SAVINGS_GOALS_LIST_URL,
                    HttpMethod.GET,
                    request,
                    new ParameterizedTypeReference<>() {
                    },
                    params
            );

            return responseEntity.getBody();
        } catch (Exception e) {
            String message = "Failed to retrieve saving goals";
            loggerUtils.logException(logger, message, e);
            throw new RoundUpException(message, e.getMessage(), e);
        }
    }

    /**
     * Create a new savings goal with the specified name.
     *
     * @param savingGoalName The name of the savings goal to create.
     * @return The response containing the created savings goal.
     * @throws RoundUpException If an error occurs while creating the savings goal.
     */
    public ResponseEntity<SavingGoalResponse> createSavingsGoal(String savingGoalName) throws RoundUpException {
        try {
            String accountUid = accountDetailsService.getAccountUid();
            Map<String, String> params = new HashMap<>();
            params.put("accountUid", accountUid);

            HttpHeaders httpHeaders = headersConfiguration.getHeaders();
            SavingGoalRequest savingGoalRequest = SavingGoalRequest.builder()
                    .name(savingGoalName)
                    .currency(Currency.getInstance("GBP"))
                    .build();
            HttpEntity<SavingGoalRequest> request = new HttpEntity<>(savingGoalRequest, httpHeaders);

            ResponseEntity<SavingGoalResponse> responseEntity = restTemplate.exchange(
                    StarlingApiUtils.API_STARLING_BANK_SAVINGS_GOALS_URL,
                    HttpMethod.PUT,
                    request,
                    SavingGoalResponse.class,
                    params
            );

            return responseEntity;
        } catch (Exception e) {
            String message = "Failed to retrieve saving goals";
            loggerUtils.logException(logger, message, e);
            throw new RoundUpException(message, e.getMessage(), e);
        }
    }

    /**
     * Add savings to a specified savings goal.
     *
     * @param savingsGoalUUID The UUID of the savings goal to add savings to.
     * @param roundedUpValue  The amount of savings to add.
     * @throws RoundUpException If an error occurs while adding savings to the savings goal.
     */
    public void putSavings(final String savingsGoalUUID, final BigDecimal roundedUpValue) throws RoundUpException {
        try {
            String accountUid = accountDetailsService.getAccountUid();
            Map<String, String> params = new HashMap<>();
            params.put("accountUid", accountUid);
            params.put("savingsGoalUid", savingsGoalUUID);
            params.put("transferUid", UUID.randomUUID().toString());

            HttpEntity<AmountResponse> request = headersConfiguration.getAmountCustomHeaders(roundedUpValue);
            restTemplate.exchange(
                    StarlingApiUtils.API_STARLING_BANK_PUT_SAVINGS_URL,
                    HttpMethod.PUT,
                    request,
                    String.class,
                    params
            );
        } catch (Exception e) {
            String message = "Failed to retrieve account ID";
            loggerUtils.logException(logger, message, e);
            throw new RoundUpException(message, e.getMessage(), e);
        }
    }
}
