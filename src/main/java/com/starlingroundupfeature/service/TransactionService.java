package com.starlingroundupfeature.service;

import com.starlingroundupfeature.configuration.HeadersConfig;
import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.feed.Feed;
import com.starlingroundupfeature.reponse.transaction.Transaction;
import com.starlingroundupfeature.util.LoggerUtils;
import com.starlingroundupfeature.util.StarlingApiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TransactionService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionService.class);

    private final RestTemplate restTemplate;
    private final HeadersConfig headersConfiguration;
    private final AccountDetailsService accountDetailsService;
    private final LoggerUtils loggerUtils;

    @Autowired
    public TransactionService(RestTemplate restTemplate, HeadersConfig headersConfiguration, AccountDetailsService accountDetailsService, LoggerUtils loggerUtils) {
        this.restTemplate = restTemplate;
        this.headersConfiguration = headersConfiguration;
        this.accountDetailsService = accountDetailsService;
        this.loggerUtils = loggerUtils;
    }

    /**
     * Get transactions.
     *
     * @return List of {@link Feed}
     * @throws RoundUpException if an error occurs while fetching transactions
     */
    public List<Feed> getTransactions() throws RoundUpException {
        try {
            final HttpEntity<Void> request = headersConfiguration.getCustomHeaders();
            final String accountUid = accountDetailsService.getAccountUid();
            final String defaultCategoryUid = accountDetailsService.getDefaultCategoryUid();

            LocalDate currentDate = LocalDate.now();
            LocalDate sevenDaysAgo = currentDate.minusDays(6);

            String formattedDate = formatDateForAPI(sevenDaysAgo);

            Map<String, String> params = new HashMap<>();
            params.put("accountUid", accountUid);
            params.put("categoryUid", defaultCategoryUid);
            params.put("formattedDate", formattedDate);

            String url = StarlingApiUtils.API_STARLING_BANK_TRANSACTIONS_URL
                    .replace("{accountUid}", accountUid)
                    .replace("{categoryUid}", defaultCategoryUid)
                    .replace("{formattedDate}", formattedDate);

            URI requestUrl = URI.create(url);
            logger.info("Request URL: {}", requestUrl);

            final Transaction response = restTemplate.exchange(StarlingApiUtils.API_STARLING_BANK_TRANSACTIONS_URL,
                    HttpMethod.GET,
                    request,
                    new ParameterizedTypeReference<Transaction>() {
                    }, params).getBody();


            logger.info("Received transactions for account: {}", accountUid);

            return response.getFeedResponseList();
        } catch (Exception e) {
            String message = "Error fetching transactions";
            loggerUtils.logException(logger, message, e);
            throw new RoundUpException("Error fetching transactions", e.getMessage(), e);
        }
    }

    public static String formatDateForAPI(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return date.atStartOfDay().format(formatter);
    }
}
