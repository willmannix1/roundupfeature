package com.starlingroundupfeature.util;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class LoggerUtils {

    /**
     * Handle an exception by logging the error.
     *
     * @param message The error message.
     * @param e       The exception that occurred.
     */
    public void logException(Logger log, String message, Exception e) {
        log.error(message, e);
    }
}
