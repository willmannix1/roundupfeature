package com.starlingroundupfeature.util;

import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.amount.Amount;
import com.starlingroundupfeature.reponse.feed.Feed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Utility class for rounding up transaction amounts.
 */
@Component
public class RoundUpUtils {

    private static final Logger LOG = LoggerFactory.getLogger(RoundUpUtils.class);

    /**
     * Calculate the total rounded-up amount from a list of transactions.
     *
     * @param transactions The list of transactions to calculate the total from.
     * @return The total rounded-up amount.
     * @throws RoundUpException If an error occurs during the calculation.
     */
    public BigDecimal calculate(List<Feed> transactions) throws RoundUpException {
        try {
            return transactions.stream()
                    .map(Feed::getAmount)
                    .map(Amount::getMinorUnits)
                    .map(this::convertToBaseUnit)
                    .filter(amount -> amount.compareTo(BigDecimal.ZERO) > 0)
                    .map(this::roundingUpAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
        } catch (Exception e) {
            LOG.error("Error occurred while calculating round-up amount", e);
            throw new RoundUpException("Error occurred while calculating round-up amount", e.getMessage(), e);
        }
    }

    /**
     * Convert the amount from minor units to the base unit (e.g., pounds).
     *
     * @param minorUnits The amount in minor units.
     * @return The amount in the base unit.
     */
    private BigDecimal convertToBaseUnit(BigDecimal minorUnits) {
        return minorUnits.divide(BigDecimal.valueOf(100));
    }

    /**
     * Round up the amount to the nearest whole number.
     *
     * @param amount The amount to be rounded up.
     * @return The rounded-up amount.
     */
    private BigDecimal roundingUpAmount(BigDecimal amount) {
        return amount.setScale(0, RoundingMode.UP).subtract(amount);
    }
}