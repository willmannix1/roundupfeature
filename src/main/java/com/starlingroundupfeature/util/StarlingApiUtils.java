package com.starlingroundupfeature.util;

public final class StarlingApiUtils {

    public static final String API_STARLING_BANK_TRANSACTIONS_URL = "https://api-sandbox.starlingbank.com/api/v2/feed/account/{accountUid}/category/{categoryUid}?changesSince={formattedDate}";
    public static final String API_STARLING_BANK_SAVINGS_GOALS_URL = "https://api-sandbox.starlingbank.com/api/v2/account/{accountUid}/savings-goals";
    public static final String API_STARLING_BANK_PUT_SAVINGS_URL = "https://api-sandbox.starlingbank.com/api/v2/account/{accountUid}/savings-goals/{savingsGoalUid}/add-money/{transferUid}";
    public static final String API_STARLING_BANK_ACCOUNT_INFO = "https://api-sandbox.starlingbank.com/api/v2/accounts";
    public static final String API_STARLING_BANK_SAVINGS_GOALS_LIST_URL = "https://api-sandbox.starlingbank.com/api/v2/account/{accountUid}/savings-goals/{savingsGoalUid}";
}