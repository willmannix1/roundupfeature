package com.starlingroundupfeature.configuration;

import com.starlingroundupfeature.reponse.amount.Amount;
import com.starlingroundupfeature.reponse.amount.AmountResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.math.BigDecimal;
import java.util.List;

class HeadersConfigTest {

    private final HeadersConfig headersConfig = new HeadersConfig();

    @Test
    void testGetHeaders() {
        HttpHeaders headers = headersConfig.getHeaders();

        Assertions.assertEquals(MediaType.APPLICATION_JSON, headers.getContentType());
        Assertions.assertEquals(List.of(MediaType.APPLICATION_JSON), headers.getAccept());
        Assertions.assertTrue(headers.containsKey("Authorization"));
        Assertions.assertTrue(headers.getFirst("Authorization").startsWith("Bearer "));
    }

    @Test
    void testGetSavingGoalsInfoCustomHeaders() {
        HttpEntity<?> entity = headersConfig.getSavingGoalsInfoCustomHeaders();

        HttpHeaders headers = entity.getHeaders();
        Assertions.assertEquals(MediaType.APPLICATION_JSON, headers.getContentType());
        Assertions.assertEquals(List.of(MediaType.APPLICATION_JSON), headers.getAccept());
        Assertions.assertTrue(headers.containsKey("Authorization"));
        Assertions.assertTrue(headers.getFirst("Authorization").startsWith("Bearer "));
    }

    @Test
    void testGetAccountInfoCustomHeaders() {
        HttpEntity<?> entity = headersConfig.getAccountInfoCustomHeaders();

        HttpHeaders headers = entity.getHeaders();
        Assertions.assertEquals(MediaType.APPLICATION_JSON, headers.getContentType());
        Assertions.assertEquals(List.of(MediaType.APPLICATION_JSON), headers.getAccept());
        Assertions.assertTrue(headers.containsKey("Authorization"));
        Assertions.assertTrue(headers.getFirst("Authorization").startsWith("Bearer "));
    }

    @Test
    void testGetAmountCustomHeaders() {
        BigDecimal roundedUpValue = BigDecimal.TEN;
        HttpEntity<?> entity = headersConfig.getAmountCustomHeaders(roundedUpValue);

        HttpHeaders headers = entity.getHeaders();
        Assertions.assertEquals(MediaType.APPLICATION_JSON, headers.getContentType());
        Assertions.assertEquals(List.of(MediaType.APPLICATION_JSON), headers.getAccept());
        Assertions.assertTrue(headers.containsKey("Authorization"));
        Assertions.assertTrue(headers.getFirst("Authorization").startsWith("Bearer "));

        Object body = entity.getBody();
        Assertions.assertTrue(body instanceof AmountResponse);

        AmountResponse amountResponse = (AmountResponse) body;
        Assertions.assertNotNull(amountResponse.getAmount());

        Amount amount = amountResponse.getAmount();
        Assertions.assertEquals(roundedUpValue.movePointRight(2), amount.getMinorUnits());
        Assertions.assertEquals("GBP", amount.getCurrency().getCurrencyCode());
    }

    @Test
    void testGetCustomHeaders() {
        HttpEntity<?> entity = headersConfig.getCustomHeaders();

        HttpHeaders headers = entity.getHeaders();
        Assertions.assertEquals(MediaType.APPLICATION_JSON, headers.getContentType());
        Assertions.assertEquals(List.of(MediaType.APPLICATION_JSON), headers.getAccept());
        Assertions.assertTrue(headers.containsKey("Authorization"));
        Assertions.assertTrue(headers.getFirst("Authorization").startsWith("Bearer "));
    }
}
