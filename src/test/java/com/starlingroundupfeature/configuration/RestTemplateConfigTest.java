package com.starlingroundupfeature.configuration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

class RestTemplateConfigTest {

    private final RestTemplateConfig restTemplateConfig = new RestTemplateConfig();

    @Test
    void testCreateRestTemplate() {
        RestTemplate restTemplate = restTemplateConfig.createRestTemplate();

        Assertions.assertNotNull(restTemplate);
    }
}