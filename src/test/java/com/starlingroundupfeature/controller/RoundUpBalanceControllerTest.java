package com.starlingroundupfeature.controller;

import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.savinggoal.SavingGoalResponse;
import com.starlingroundupfeature.service.RoundUpBalanceService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RoundUpBalanceControllerTest {

    private MockMvc mockMvc;

    @Mock
    private RoundUpBalanceService roundUpBalanceService;

    @InjectMocks
    private RoundUpBalanceController roundUpBalanceController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(roundUpBalanceController).build();
    }

    @Test
    public void testRoundUpBalanceController() throws Exception {
        // Mock the behavior of RoundUpBalanceService
        SavingGoalResponse mockResponse = new SavingGoalResponse();
        mockResponse.setSavingsGoalUid("testSavingGoalUid");
        when(roundUpBalanceService.roundUp(anyString())).thenReturn(mockResponse);

        // Perform GET request to /round-up endpoint
        mockMvc.perform(get("/starling-bank-savings/round-up"))
                .andExpect(status().isOk());
    }

    @Test
    public void testRoundUpBalanceControllerException() throws Exception {
        // Mock the behavior of RoundUpBalanceService to throw RoundUpException
        when(roundUpBalanceService.roundUp(anyString())).thenThrow(new RoundUpException("Error", "Detailed error message", new Throwable()));

        // Perform GET request to /round-up endpoint
        mockMvc.perform(get("/starling-bank-savings/round-up"))
                .andExpect(status().isInternalServerError());
    }
}