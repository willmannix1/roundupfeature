package com.starlingroundupfeature.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RoundUpExceptionTest {

    @Test
    void testRoundUpException() {
        String message = "Test exception message";
        String detailedMessage = "Detailed error message";

        RoundUpException exception = RoundUpException.builder()
                .message(message)
                .detailedMessage(detailedMessage)
                .cause(new RuntimeException())
                .build();

        Assertions.assertEquals(message, exception.getMessage());
        Assertions.assertEquals(detailedMessage, exception.getDetailedMessage());
        Assertions.assertNotNull(exception.getCause());
    }
}
