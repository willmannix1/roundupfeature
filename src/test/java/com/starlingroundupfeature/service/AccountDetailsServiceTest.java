package com.starlingroundupfeature.service;

import com.starlingroundupfeature.configuration.HeadersConfig;
import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.account.Account;
import com.starlingroundupfeature.reponse.account.AccountsResponse;
import com.starlingroundupfeature.util.LoggerUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Objects;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountDetailsServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private HeadersConfig headersConfiguration;

    @Mock
    private LoggerUtils loggerUtils;

    @InjectMocks
    private AccountDetailsService accountDetailsService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        ReflectionTestUtils.setField(accountDetailsService, "restTemplate", restTemplate);
        ReflectionTestUtils.setField(accountDetailsService, "headersConfiguration", headersConfiguration);
        ReflectionTestUtils.setField(accountDetailsService, "loggerUtils", loggerUtils);
    }

    @Test
    public void testGetAccountUid() throws RoundUpException {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<AccountsResponse> request = new HttpEntity<>(headers);

        Account account = new Account();
        account.setAccountUid("mockAccountUid");
        AccountsResponse accountsResponse = new AccountsResponse();
        accountsResponse.setAccounts(Collections.singletonList(account));

        ResponseEntity<AccountsResponse> responseEntity = ResponseEntity.ok(accountsResponse);

        when(headersConfiguration.getAccountInfoCustomHeaders()).thenReturn(request);

        when(restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                any(),
                any(ParameterizedTypeReference.class)
        )).thenReturn(responseEntity);

        String accountUid = accountDetailsService.getAccountUid();

        Assertions.assertEquals("mockAccountUid", accountUid);
    }

    @Test(expected = RoundUpException.class)
    public void testGetAccountUidThrowsException() throws RoundUpException {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<AccountsResponse> request = new HttpEntity<>(headers);

        when(headersConfiguration.getAccountInfoCustomHeaders()).thenReturn(request);
        when(restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                eq(request),
                eq(AccountsResponse.class)
        )).thenThrow(new RuntimeException("Test Exception"));

        accountDetailsService.getAccountUid();
    }

    @Test
    public void testGetDefaultCategoryUid() throws RoundUpException {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<AccountsResponse> request = new HttpEntity<>(headers);

        Account account = new Account();
        account.setDefaultCategory("mockDefaultCategoryUid");
        AccountsResponse accountsResponse = new AccountsResponse();
        accountsResponse.setAccounts(Collections.singletonList(account));

        ResponseEntity<AccountsResponse> responseEntity = ResponseEntity.ok(accountsResponse);

        when(headersConfiguration.getAccountInfoCustomHeaders()).thenReturn(request);
        when(restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                any(),
                any(ParameterizedTypeReference.class)
        )).thenReturn(responseEntity);

        String defaultCategoryUid = accountDetailsService.getDefaultCategoryUid();

        Assertions.assertEquals("mockDefaultCategoryUid", defaultCategoryUid);
    }

    @Test(expected = RoundUpException.class)
    public void testGetDefaultCategoryUidThrowsException() throws RoundUpException {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<AccountsResponse> request = new HttpEntity<>(headers);

        when(headersConfiguration.getAccountInfoCustomHeaders()).thenReturn(request);
        when(restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                eq(request),
                eq(AccountsResponse.class)
        )).thenThrow(new RuntimeException("Test Exception"));

        accountDetailsService.getDefaultCategoryUid();
    }

    @Test
    public void testGetAccountInfo() throws RoundUpException {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Void> request = new HttpEntity<>(headers);

        Account account = new Account();
        account.setAccountUid("mockAccountUid");
        AccountsResponse accountsResponse = new AccountsResponse();
        accountsResponse.setAccounts(Collections.singletonList(account));

        ResponseEntity<AccountsResponse> responseEntity = ResponseEntity.ok(accountsResponse);

        when(headersConfiguration.getCustomHeaders()).thenReturn(request);
        when(restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                any(),
                any(ParameterizedTypeReference.class)
        )).thenReturn(responseEntity);

        AccountsResponse returnedResponse = accountDetailsService.getAccountInfo();

        Assertions.assertEquals("mockAccountUid", Objects.requireNonNull(returnedResponse.getAccounts().get(0)).getAccountUid());
    }

    @Test(expected = RoundUpException.class)
    public void testGetAccountInfoThrowsException() throws RoundUpException {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Void> request = new HttpEntity<>(headers);

        when(headersConfiguration.getCustomHeaders()).thenReturn(request);
        when(restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                any(),
                any(ParameterizedTypeReference.class)
        )).thenThrow(new RuntimeException("Test Exception"));

        accountDetailsService.getAccountInfo();
    }

}