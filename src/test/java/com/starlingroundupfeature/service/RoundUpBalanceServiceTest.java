package com.starlingroundupfeature.service;

import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.amount.Amount;
import com.starlingroundupfeature.reponse.feed.Feed;
import com.starlingroundupfeature.reponse.savinggoal.SavingGoalResponse;
import com.starlingroundupfeature.util.RoundUpUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class RoundUpBalanceServiceTest {

    @Mock
    private TransactionService transactionService;

    @Mock
    private SavingGoalService savingGoalService;

    @Mock
    private RoundUpUtils roundUpUtils;

    @InjectMocks
    private RoundUpBalanceService roundUpBalanceService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRoundUp() throws RoundUpException {
        // Mock transactions
        List<Feed> mockTransactions = new ArrayList<>();
        mockTransactions.add(createMockTransaction(BigDecimal.valueOf(1200))); // £12.00
        mockTransactions.add(createMockTransaction(BigDecimal.valueOf(560)));  // £5.60
        mockTransactions.add(createMockTransaction(BigDecimal.valueOf(840)));  // £8.40

        // Mock round-up calculation
        when(transactionService.getTransactions()).thenReturn(mockTransactions);
        when(roundUpUtils.calculate(any())).thenReturn(BigDecimal.valueOf(26.00)); // £26.00

        // Mock saving goal service
        SavingGoalResponse mockSavingGoalResponse = new SavingGoalResponse();
        mockSavingGoalResponse.setSavingsGoalUid("mockUid");
        when(savingGoalService.createSavingsGoal(anyString())).thenReturn(ResponseEntity.ok(mockSavingGoalResponse));

        // Mock saving goal service for getting goal after round-up
        when(savingGoalService.getSavingsGoals("mockUid")).thenReturn(mockSavingGoalResponse);


        // Perform round-up
        SavingGoalResponse result = roundUpBalanceService.roundUp("TestSavingGoal");

        // Assert result
        assertNotNull(result);
        assertEquals("mockUid", result.getSavingsGoalUid());
    }

    private Feed createMockTransaction(BigDecimal amountInMinorUnits) {
        Feed feed = new Feed();
        feed.setAmount(createMockAmount(amountInMinorUnits));
        return feed;
    }

    private Amount createMockAmount(BigDecimal amountInMinorUnits) {
        Amount amount = new Amount();
        amount.setMinorUnits(amountInMinorUnits);
        return amount;
    }
}