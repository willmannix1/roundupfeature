package com.starlingroundupfeature.service;

import com.starlingroundupfeature.configuration.HeadersConfig;
import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.amount.AmountResponse;
import com.starlingroundupfeature.reponse.savinggoal.SavingGoalResponse;
import com.starlingroundupfeature.util.LoggerUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Map;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SavingGoalServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private HeadersConfig headersConfiguration;

    @Mock
    private AccountDetailsService accountDetailsService;

    @Mock
    private LoggerUtils loggerUtils;

    // Injecting mocks into the SavingGoalService instance
    @InjectMocks
    private SavingGoalService savingGoalService;

    @Before
    public void setUp() {
        // Initialize Mockito annotations
        MockitoAnnotations.initMocks(this);

        // Inject mock dependencies
        ReflectionTestUtils.setField(savingGoalService, "restTemplate", restTemplate);
        ReflectionTestUtils.setField(savingGoalService, "headersConfiguration", headersConfiguration);
        ReflectionTestUtils.setField(savingGoalService, "accountDetailsService", accountDetailsService);
        ReflectionTestUtils.setField(savingGoalService, "loggerUtils", loggerUtils);
    }

    @Test
    public void testPutSavings() throws RoundUpException {
        when(accountDetailsService.getAccountUid()).thenReturn("mockAccountUid");

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<AmountResponse> request = new HttpEntity<>(headers);
        BigDecimal roundedUpValue = BigDecimal.TEN;
        when(headersConfiguration.getAmountCustomHeaders(roundedUpValue)).thenReturn(request);

        ResponseEntity<String> responseEntity = ResponseEntity.ok().build();
        when(restTemplate.exchange(anyString(), eq(HttpMethod.PUT), eq(request), eq(String.class), anyMap()))
                .thenReturn(responseEntity);

        savingGoalService.putSavings("mockUid", roundedUpValue);

        verify(accountDetailsService, times(1)).getAccountUid();

        verify(restTemplate, times(1)).exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class), anyMap());
    }

    @Test(expected = RoundUpException.class)
    public void testPutSavingsThrowsException() throws RoundUpException {
        // Mocking the account UID retrieval to throw an exception
        when(accountDetailsService.getAccountUid()).thenThrow((new RuntimeException("Test Exception")));

        savingGoalService.putSavings("mockUid", BigDecimal.TEN);
    }

    @Test
    public void testCreateSavingsGoal() throws RoundUpException {
        // Mocking
        SavingGoalResponse expectedResponse = new SavingGoalResponse();
        expectedResponse.setSavingsGoalUid("mockUid");
        when(restTemplate.exchange(anyString(), eq(HttpMethod.PUT), any(), eq(SavingGoalResponse.class), anyMap()))
                .thenReturn(ResponseEntity.ok(expectedResponse));

        ResponseEntity<SavingGoalResponse> responseEntity = savingGoalService.createSavingsGoal("Test Goal");

        Assertions.assertNotNull(responseEntity);
        Assertions.assertEquals("mockUid", responseEntity.getBody().getSavingsGoalUid());
    }

    @Test(expected = RoundUpException.class)
    public void testCreateSavingsGoalThrowsException() throws RoundUpException {
        when(restTemplate.exchange(anyString(), eq(HttpMethod.PUT), any(), eq(SavingGoalResponse.class), anyMap()))
                .thenThrow(new RuntimeException("Test Exception"));

        savingGoalService.createSavingsGoal("Test Goal");
    }

    @Test
    public void testGetSavingsGoals() throws RoundUpException {
        when(accountDetailsService.getAccountUid()).thenReturn("mockAccountUid");

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<SavingGoalResponse> request = new HttpEntity<>(headers);

        SavingGoalResponse expectedResponse = new SavingGoalResponse();
        expectedResponse.setSavingsGoalUid("mockSavingsGoalUid");
        ResponseEntity<SavingGoalResponse> responseEntity = ResponseEntity.ok(expectedResponse);
        when(restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                any(),
                any(ParameterizedTypeReference.class),
                any(Map.class)
        )).thenReturn(responseEntity);

        SavingGoalResponse actualResponse = savingGoalService.getSavingsGoals("mockSavingsGoalUid");

        verify(accountDetailsService, times(1)).getAccountUid();

        verify(restTemplate, times(1)).exchange(anyString(), eq(HttpMethod.GET), any(), any(ParameterizedTypeReference.class), any(Map.class));

        Assertions.assertNotNull(actualResponse);
        Assertions.assertEquals("mockSavingsGoalUid", actualResponse.getSavingsGoalUid());
    }

    @Test(expected = RoundUpException.class)
    public void testGetSavingsGoalsThrowsException() throws RoundUpException {
        // Mocking the account UID retrieval
        when(accountDetailsService.getAccountUid()).thenReturn("mockAccountUid");

        // Mocking the headers and request
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<SavingGoalResponse> request = new HttpEntity<>(headers);

        // Mocking restTemplate.exchange to throw an exception
        when(restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                any(),
                any(ParameterizedTypeReference.class),
                any(Map.class)
        )).thenThrow(new RuntimeException("Test Exception"));

        // Call the method under test
        savingGoalService.getSavingsGoals("mockSavingsGoalUid");

        // Verify that accountDetailsService.getAccountUid is called only once
        verify(accountDetailsService, times(1)).getAccountUid();

        // Verify that restTemplate.exchange is called only once
        verify(restTemplate, times(1)).exchange(anyString(), eq(HttpMethod.GET), any(), any(ParameterizedTypeReference.class), any(Map.class));
    }

}
