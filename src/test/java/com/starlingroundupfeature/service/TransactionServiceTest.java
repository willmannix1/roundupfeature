package com.starlingroundupfeature.service;

import com.starlingroundupfeature.configuration.HeadersConfig;
import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.account.Account;
import com.starlingroundupfeature.reponse.account.AccountsResponse;
import com.starlingroundupfeature.reponse.feed.Feed;
import com.starlingroundupfeature.reponse.transaction.Transaction;
import com.starlingroundupfeature.util.LoggerUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class TransactionServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private HeadersConfig headersConfiguration;

    @Mock
    private AccountDetailsService accountDetailsService;

    @Mock
    private LoggerUtils loggerUtils;

    @InjectMocks
    private TransactionService transactionService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        // Inject mock dependencies
        ReflectionTestUtils.setField(transactionService, "restTemplate", restTemplate);
        ReflectionTestUtils.setField(transactionService, "headersConfiguration", headersConfiguration);
        ReflectionTestUtils.setField(transactionService, "accountDetailsService", accountDetailsService);
        ReflectionTestUtils.setField(transactionService, "loggerUtils", loggerUtils);
    }

    @Test
    public void testGetTransactions() throws RoundUpException {
        // Stubbing for AccountDetailsService
        when(accountDetailsService.getAccountUid()).thenReturn("mockAccountId");
        when(accountDetailsService.getDefaultCategoryUid()).thenReturn("mockCategoryUid");

        HttpEntity<Void> customHeaders = new HttpEntity<>(new HttpHeaders());
        when(headersConfiguration.getCustomHeaders()).thenReturn(customHeaders);

        // Mock response for RestTemplate
        Transaction transaction = new Transaction();
        Feed mockFeed = new Feed();
        List<Feed> mockFeedList = Collections.singletonList(mockFeed);
        transaction.setFeedResponseList(mockFeedList);
        ResponseEntity<Transaction> transactionResponseEntity = ResponseEntity.ok(transaction);
        when(restTemplate.exchange(
                anyString(),
                eq(HttpMethod.GET),
                any(),
                any(ParameterizedTypeReference.class),
                any(Map.class))
        ).thenReturn(transactionResponseEntity);

        List<Feed> result = transactionService.getTransactions();

        verify(restTemplate).exchange(anyString(), eq(HttpMethod.GET), any(), any(ParameterizedTypeReference.class), any(Map.class));

        // Assert the result
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test(expected = RoundUpException.class)
    public void testGetTransactionsThrowsException() throws RoundUpException {
        // Mocking
        AccountsResponse accountsResponse = new AccountsResponse();
        Account account = new Account();
        account.setAccountUid("mockAccountId");
        accountsResponse.setAccounts(Collections.singletonList(account));
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(), eq(AccountsResponse.class)))
                .thenReturn(ResponseEntity.ok(accountsResponse));

        when(headersConfiguration.getCustomHeaders()).thenReturn(new HttpEntity<>(new HttpHeaders()));

        TransactionService transactionServiceSpy = spy(transactionService);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(), eq(Feed.class), any(Object[].class)))
                .thenThrow(new RuntimeException("Test Exception"));

        // Invocation
        transactionServiceSpy.getTransactions();
    }
}
