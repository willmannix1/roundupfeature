package com.starlingroundupfeature.util;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;

class LoggerUtilsTest {

    @Test
    void testLogException() {
        LoggerUtils loggerUtils = new LoggerUtils();
        Logger mockLogger = Mockito.mock(Logger.class);
        Exception mockException = new RuntimeException("Test exception");

        loggerUtils.logException(mockLogger, "Test error message", mockException);

        Mockito.verify(mockLogger).error("Test error message", mockException);
    }
}