package com.starlingroundupfeature.util;

import com.starlingroundupfeature.exception.RoundUpException;
import com.starlingroundupfeature.reponse.amount.Amount;
import com.starlingroundupfeature.reponse.feed.Feed;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

public class RoundUpUtilsTest {

    @Mock
    private Feed mockFeed;

    @InjectMocks
    private RoundUpUtils roundUpUtils;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCalculate() throws RoundUpException {
        // Create real Feed objects
        Feed feed1 = new Feed();
        Feed feed2 = new Feed();

        // Create and set Amount objects for each Feed
        Amount amount1 = new Amount();
        amount1.setMinorUnits(BigDecimal.valueOf(123));
        feed1.setAmount(amount1);

        Amount amount2 = new Amount();
        amount2.setMinorUnits(BigDecimal.valueOf(123));
        feed2.setAmount(amount2);

        // Mock data
        List<Feed> transactions = Arrays.asList(feed1, feed2);

        // Call the method under test
        BigDecimal result = roundUpUtils.calculate(transactions);

        // Assert the result
        Assertions.assertEquals(BigDecimal.valueOf(1.54), result);
    }


    @Test(expected = RoundUpException.class)
    public void testCalculateWithError() throws RoundUpException {
        when(mockFeed.getAmount()).thenReturn(new Amount());
        // Mock data
        List<Feed> transactions = Arrays.asList(mockFeed, mockFeed);

        // Mock minor units to cause an exception
        when(mockFeed.getAmount().getMinorUnits()).thenThrow(new RuntimeException("Test Exception"));

        // Call the method under test
        roundUpUtils.calculate(transactions);
    }

    @Test
    public void testCalculateWithZeroAmount() throws RoundUpException {
        // Create real Feed objects
        Feed feed1 = new Feed();
        Feed feed2 = new Feed();

        // Create and set Amount objects for each Feed
        Amount amount1 = new Amount();
        amount1.setMinorUnits(BigDecimal.ZERO);
        feed1.setAmount(amount1);

        Amount amount2 = new Amount();
        amount2.setMinorUnits(BigDecimal.ZERO);
        feed2.setAmount(amount2);

        // Mock data
        List<Feed> transactions = Arrays.asList(feed1, feed2);

        // Call the method under test
        BigDecimal result = roundUpUtils.calculate(transactions);

        // Assert the result is zero
        Assertions.assertEquals(BigDecimal.ZERO, result);
    }

}
